FROM php:7.3-apache

RUN apt-get update \
 && apt-get install -y git zip libzip-dev zlib1g-dev libpq-dev \
 && docker-php-ext-configure zip --with-libzip \
 && docker-php-ext-install zip pdo_pgsql \
 && a2enmod rewrite \
 && sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf \
 && mv /var/www/html /var/www/public \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www
