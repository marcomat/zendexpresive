<?php

namespace Alumno;

use Alumno\Handler;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;

class RoutesDelegator
{

    /**
     * @param ContainerInterface $container
     * @param string $serviceName Name of the service being created.
     * @param callable $callback Creates and eturns the service.
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $serviceName, callable $callback)
    {
        // TODO: Implement __invoke() method.
        $app=$callback();

        $app->get('/Alumno[/]', \Alumno\Handler\Alumno::class,'alumno.read');

        return $app;
    }
}