<?php

declare(strict_types=1);

namespace Alumno\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

class Alumno implements RequestHandlerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        // Create and return a response
        $result['Alumno'] = ['codigo' => '1', 'first_name'=>'Marco', 'last_name'=>'Matamoros', 'facultad'=>'Sistemas'];

        return new JsonResponse($result);
    }
}
