<?php

declare(strict_types=1);

namespace Alumno\Handler;

use Psr\Container\ContainerInterface;

class AlumnoFactory
{
    public function __invoke(ContainerInterface $container) : Alumno
    {
        return new Alumno();
    }
}
