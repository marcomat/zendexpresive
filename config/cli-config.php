<?php




use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Symfony\Component\Console\Helper\HelperSet;


/** @var TYPE_NAME $container */
$container = require __DIR__ .'/container.php';

/** @var TYPE_NAME $container */
return new HelperSet([
   'em' => new EntityManagerHelper(
       $container->get(EntityManager::class)//set in App/ConfigProvider.php
   ),
]);
